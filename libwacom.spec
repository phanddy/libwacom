Name:           libwacom
Version:        1.4.1
Release:        1
Summary:        Wacom Tablets Library
Requires:       %{name}-data
License:        MIT
URL:            https://github.com/linuxwacom/libwacom
Source0:        https://github.com/linuxwacom/libwacom/releases/download/%{name}-%{version}/%{name}-%{version}.tar.bz2

BuildRequires:  autoconf automake libtool doxygen glib2-devel libgudev1-devel
BuildRequires:  systemd systemd-devel git

%global udevdir /usr/lib/udev/

%description
libwacom is a library to identify wacom tablets and their model-specific
features. 

%package devel
Summary:        Tablet Information Client Library Library Development Package
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig

%description devel
Development package for libwacom.

%package data
Summary:        Tablet Information Client Library Library Data Files

%description data
Tablet information client library library data files.

%prep
%autosetup -n %{name}-%{version} -p1 -Sgit

%build
autoreconf --force -v --install || exit 1
%configure --disable-static --disable-silent-rules
%make_build

%install
%make_install INSTALL="install -p"
install -d ${RPM_BUILD_ROOT}/%{_udevrulesdir}
./tools/generate-hwdb > ${RPM_BUILD_ROOT}/%{_udevrulesdir}/65-libwacom.rules


%check
make %{?_smp_mflags} check

%ldconfig_scriptlets

%files
%{_libdir}/libwacom.so.*
%{_libdir}/udev/hwdb.d/*
%{_libdir}/udev/rules.d/*
%{udevdir}/rules.d/*
%{_bindir}/libwacom-list-local-devices
%{_mandir}/man1/libwacom-list-local-devices.1*
%license COPYING
%doc README.md
%exclude %{_libdir}/*.la

%files devel
%{_libdir}/libwacom.so
%{_libdir}/pkgconfig/libwacom.pc
%{_includedir}/libwacom-1.0/libwacom/libwacom.h

%files data
%doc COPYING
%{_datadir}/libwacom/*.tablet
%{_datadir}/libwacom/*.stylus
%{_datadir}/libwacom/layouts/*.svg

%changelog
* Tue Jul 21 2020 dingyue <dingyue5@openeuler.org> - 1.4.1-1
- Package update
* Tue Aug 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.31-2
- Package init
